# Abstract

In this paper, we propose a multimodal kernel semantic embedding method to address cross-modal 
retrieval problems. This method allows cross-modal retrieval by aligning the semantic projections of 
each modality. These semantic projections follow a nonlinear modeling by using a kernel matrix 
factorization strategy. Furthermore, this method can be used in a supervised way by taking advantage 
of extra annotated semantic information in order to enrich the common semantic representation or even 
model a new semantic space based on the annotation labels. One of the remarkable aspects of the proposed 
method is its scalable architecture, which despite being based on kernel methods scales well to deal 
with large datasets, thanks to its learning-in-budget strategy and its formulation as an end-to-end 
architecture that can be efficiently trained using online learning based on stochastic gradient descent. 
Experiments over a publicly available dataset show that the proposed method exhibits a significant 
improvement over the results obtained in the state of the art.